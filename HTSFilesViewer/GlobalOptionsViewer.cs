﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using FileBasicAnalysis;

namespace HTSFilesViewer
{
    public partial class GlobalOptionsViewer : UserControl
    {
        [Browsable(true)]
        public event EventHandler Changed;

        HTSVoiceFile _file;

        public GlobalOptionsViewer()
        {
            InitializeComponent();
        }

        public HTSVoiceFile HTSVoiceFile
        {
            get { return _file; }
            set
            {
                _file = value;

                if (_file != null)
                {
                    txtHTSVoiceVer.Text = _file.HTSVoiceVer;
                    txtSamplingFrq.Text = _file.SamplingFrequency.ToString();
                    txtFramePrd.Text = _file.FramePeriod.ToString();
                    txtNumStates.Text = _file.NumStates.ToString();
                    txtNumStreams.Text = _file.NumStreams.ToString();
                    chkUseLPF.Checked = _file.UseLPFStream;
                    txtLabelFmt.Text = _file.LabelFormat;
                    txtLabelVer.Text = _file.LabelVersion;
                    txtComment.Text = _file.Comment;
                }                
            }
        }

        private void txtSamplingFrq_Validating(object sender, CancelEventArgs e)
        {
            TextBox sendertext = sender as TextBox;
            int dummy;

            e.Cancel = false;

            switch (sendertext.Name)
            {
                case "txtSamplingFrq":
                case "txtFramePrd":
                case "txtNumStates":
                    e.Cancel = !Int32.TryParse(sendertext.Text, out dummy);
                    break;
            }
        }

        private void txtSamplingFrq_Validated(object sender, EventArgs e)
        {
            TextBox sendertext = sender as TextBox;
            bool changed = false;

            switch (sendertext.Name)
            {
                case "txtSamplingFrq":
                    if (_file.SamplingFrequency != Int32.Parse(sendertext.Text))
                    {
                        _file.SamplingFrequency = Int32.Parse(sendertext.Text);
                        changed = true;
                    }
                    break;
                case "txtFramePrd":
                    if (_file.FramePeriod != Int32.Parse(sendertext.Text))
                    {
                        _file.FramePeriod = Int32.Parse(sendertext.Text);
                        changed = true;
                    }
                    break;
                case "txtNumStates":
                    if (_file.NumStates != Int32.Parse(sendertext.Text))
                    {
                        _file.NumStates = Int32.Parse(sendertext.Text);
                        changed = true;
                    }
                    break;
                case "txtLabelFmt":
                    if (_file.LabelFormat != sendertext.Text)
                    {
                        _file.LabelFormat = sendertext.Text;
                        changed = true;
                    }
                    break;
                case "txtLabelVer":
                    if (_file.LabelVersion != sendertext.Text)
                    {
                        _file.LabelVersion = sendertext.Text;
                        changed = true;
                    }
                    break;
                case "txtGVOff":
                    if (_file.GVOffForEditing != sendertext.Text)
                    {
                        _file.SetGVOffByEditing(sendertext.Text);
                        changed = true;
                    }
                    break;
                case "txtComment":
                    if (_file.Comment != sendertext.Text)
                    {
                        _file.Comment = sendertext.Text;
                        changed = true;
                    }
                    break;
            }

            if (changed && this.Changed != null)
            {
                Changed(this, new EventArgs());
            }
        }

        private void chkUseLPF_CheckedChanged(object sender, EventArgs e)
        {
            _file.UseLPFStream = chkUseLPF.Checked;

            if (this.Changed != null)
            {
                Changed(this, new EventArgs());
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            chkUseLPF.Checked = !chkUseLPF.Checked;
        }
    }
}
