﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace FileBasicAnalysis
{
    public class HTSPDFFile
    {
        int[] leaves;
        TreePDF[] _treepdfs;
        //LeafPDF[] _leafpdfs;
        bool _msd;
        int _windows;
        int _dim;

        public HTSPDFFile(FileStream htsvoice, long start, long end,
            int num_states, bool is_msd, int num_windows, int vector_len)
        {
            int i, j;
            int total_leaves = 0;
            BinaryReader reader = new BinaryReader(htsvoice);

            reader.BaseStream.Seek(start, SeekOrigin.Current);

            _msd = is_msd; _windows = num_windows; _dim = vector_len;
            leaves = new int[num_states];
            for (i = 0; i < num_states; i++)
                total_leaves += (leaves[i] = reader.ReadInt32());

            if (sizeof(float) * //length of float
                (leaves.Length + //floats in header
                    total_leaves * //number of leaves
                    ((is_msd ? 1 : 0) + 2 * num_windows * vector_len)
                )
                ==
                end - start + 1)
            {
                _treepdfs = new TreePDF[num_states];
                for (i = 0; i < num_states; i++)
                {
                    _treepdfs[i] = new TreePDF(leaves[i], _msd, _windows, _dim);
                    _treepdfs[i].Read(reader);
                }

                reader.BaseStream.Seek(-end - 1, SeekOrigin.Current);
            }
            else
            {
                reader.BaseStream.Seek(-4 * num_states - start, SeekOrigin.Current);
                throw new InvalidDataException();
            }
        }

        public class LeafPDF
        {
            float[,] averages; //[window, dim]
            float[,] variances; //[window, dim]
            float msd_weight;

            public LeafPDF(bool is_msd, int num_windows, int vector_len)
            {
                averages = new float[num_windows, vector_len];
                variances = new float[num_windows, vector_len];
                msd_weight = is_msd ? 0 : -1;
            }

            public void Read(BinaryReader reader)
            {
                int i, j;
                for (i = 0; i <= averages.GetUpperBound(0); i++)
                    for (j = 0; j <= averages.GetUpperBound(1); j++)
                        averages[i, j] = reader.ReadSingle();
                for (i = 0; i <= variances.GetUpperBound(0); i++)
                    for (j = 0; j <= variances.GetUpperBound(1); j++)
                        variances[i, j] = reader.ReadSingle();
                if (msd_weight >= 0) msd_weight = reader.ReadSingle();
            }
        }

        public class TreePDF
        {
            LeafPDF[] _leaves;

            public TreePDF(int leaves, bool is_msd, int num_windows, int vector_len)
            {
                _leaves = new LeafPDF[leaves];
                for (int i = 0; i < leaves; i++)
                {
                    _leaves[i] = new LeafPDF(is_msd, num_windows, vector_len);
                }
            }

            public void Read(BinaryReader reader)
            {
                for (int i = 0; i < _leaves.Length; i++)
                {
                    _leaves[i].Read(reader);
                }
            }
        }
    }
}
