﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace FileBasicAnalysis
{
    public class HTSWindowFile
    {
        float[] _coeffs;

        public HTSWindowFile(FileStream htsvoice, long start, long end)
        {
            byte[] data = new byte[end - start + 1];

            htsvoice.Seek(start, SeekOrigin.Current);
            htsvoice.Read(data, (int)0, (int)(end - start + 1));

            htsvoice.Seek(-end - 1, SeekOrigin.Current);

            //parsing
            MemoryStream stream = new MemoryStream(data);
            TextReader reader = new StreamReader(stream);

            string[] tokens =
                reader.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (Int32.Parse(tokens[0]) == tokens.Length - 1)
            {
                _coeffs = new float[tokens.Length - 1];
                for (int i = 0; i < _coeffs.Length; i++)
                    _coeffs[i] = Single.Parse(tokens[i + 1]);
            }
            else
            {
                throw new InvalidDataException();
            }
        }
    }
}
