﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace FileBasicAnalysis
{
    public class HTSTreeFile
    {
        byte[] _data;

        public HTSTreeFile(FileStream htsvoice, long start, long end)
        {
            _data = new byte[end - start + 1];

            htsvoice.Seek(start, SeekOrigin.Current);
            htsvoice.Read(_data, (int)0, (int)(end - start + 1));

            htsvoice.Seek(-end - 1, SeekOrigin.Current);

            //parsing
            MemoryStream stream = new MemoryStream(_data);
            TextReader reader = new StreamReader(stream);

        }
    }
}
