﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace FileBasicAnalysis
{
    public class HTSVoiceFile
    {
        #region string constants
        const string CHUNK_GLOBAL = "[GLOBAL]";
        const string OPTION_HTSVOICE_VER = "HTS_VOICE_VERSION";
        const string OPTION_SAMPLE_FRQ = "SAMPLING_FREQUENCY";
        const string OPTION_FRAME_PRD = "FRAME_PERIOD";
        const string OPTION_NUM_STATES = "NUM_STATES";
        const string OPTION_NUM_STREAMS = "NUM_STREAMS";
        const string OPTION_STREAM_TYPE = "STREAM_TYPE";
        const string OPTION_LABEL_FMT = "FULLCONTEXT_FORMAT";
        const string OPTION_LABEL_VER = "FULLCONTEXT_VERSION";
        const string OPTION_GV_OFF = "GV_OFF_CONTEXT";
        const string OPTION_COMMENT = "COMMENT";

        const string CHUNK_STREAM = "[STREAM]";
        const string OPTION_VECTOR_LEN = "VECTOR_LENGTH";
        const string OPTION_IS_MSD = "IS_MSD";
        const string OPTION_NUM_WINDOWS = "NUM_WINDOWS";
        const string OPTION_USE_GV = "USE_GV";
        const string OPTION_STREAM_OPT = "OPTION";
        const string OPTION_MCP_ALPHA = "ALPHA";
        const string OPTION_MCP_GAMMA = "GAMMA";
        const string OPTION_MCP_LN_GAIN = "LN_GAIN";

        const string STREAM_MCP = "MCP";
        const string STREAM_LF0 = "LF0";
        const string STREAM_LPF = "LPF";

        const string CHUNK_POSITION = "[POSITION]";
        const string SUBCHUNK_DUR_PDF = "DURATION_PDF";
        const string SUBCHUNK_DUR_TREE = "DURATION_TREE";
        const string SUBCHUNK_STM_WIN = "STREAM_WIN";
        const string SUBCHUNK_STM_PDF = "STREAM_PDF";
        const string SUBCHUNK_STM_TREE = "STREAM_TREE";
        const string SUBCHUNK_GV_PDF = "GV_PDF";
        const string SUBCHUNK_GV_TREE = "GV_TREE";

        const string CHUNK_DATA = "[DATA]";
        #endregion

        #region global options
        string _htsvoice_ver = "1.0";
        int _sampling_frq = 48000;
        int _frame_prd = 240;
        int _num_states = 5;
        string[] _stream_type = new string[] { "MCP", "LF0", "LPF" };
        string _label_fmt = "HTS_TTS_JPN";
        string _label_ver = "1.0";
        string[] _gv_off = new string[] { "\"*-sil+*\"", "\"*-pau+*\"" };
        string _comment = "";
        #endregion

        #region stream options
        int _mcp_vector_len, _lf0_vector_len, _lpf_vector_len;
        bool _mcp_is_msd, _lf0_is_msd, _lpf_is_msd;
        int _mcp_num_windows, _lf0_num_windows, _lpf_num_windows;
        bool _mcp_use_gv, _lf0_use_gv, _lpf_use_gv;
        string[] _mcp_options, _lf0_options, _lpf_options;

        string _mcp_alpha = null, _mcp_ln_gain = null, _mcp_gamma = null;
        #endregion

        #region subchunk positions
        Tuple<long, long> dur_pdf_pos, dur_tree_pos;
        Tuple<long, long> mcp_pdf_pos, mcp_tree_pos;
        Tuple<long, long> lf0_pdf_pos, lf0_tree_pos;
        Tuple<long, long> lpf_pdf_pos, lpf_tree_pos;
        Tuple<long, long>[] mcp_win_pos;
        Tuple<long, long> gv_mcp_pdf_pos, gv_mcp_tree_pos;
        Tuple<long, long>[] lf0_win_pos;
        Tuple<long, long> gv_lf0_pdf_pos, gv_lf0_tree_pos;
        Tuple<long, long>[] lpf_win_pos;
        Tuple<long, long> gv_lpf_pdf_pos, gv_lpf_tree_pos;
        #endregion

        #region subchunk members
        HTSPDFFile dur_pdf;
        HTSTreeFile dur_tree;
        HTSWindowFiles mcp_windows, lf0_windows, lpf_windows;
        HTSPDFFile mcp_pdf, lf0_pdf, lpf_pdf;
        HTSTreeFile mcp_tree, lf0_tree, lpf_tree;
        HTSPDFFile gv_mcp_pdf, gv_lf0_pdf, gv_lpf_pdf;
        HTSTreeFile gv_mcp_tree, gv_lf0_tree, gv_lpf_tree;
        #endregion

        public HTSVoiceFile(string path)
        {
            FileStream htsvoice = new FileStream(path, FileMode.Open);
            TextReader reader = new StreamReader(htsvoice, Encoding.ASCII);

            string line = reader.ReadLine();
            int i = 0;

            int position = line.Length + 1;

            if (line != CHUNK_GLOBAL) throw new InvalidDataException();

            #region Reading GLOBAL options
            while ((line = reader.ReadLine()) != CHUNK_STREAM)
            {
                position += line.Length + 1;

                if (line.StartsWith(OPTION_HTSVOICE_VER))
                {
                    _htsvoice_ver = line.Substring(OPTION_HTSVOICE_VER.Length + 1).TrimStart();
                    if (_htsvoice_ver != "1.0")
                    {
                        //throw exception? just give warning?
                        //throw new NotSupportedException();
                    }
                }
                else if (line.StartsWith(OPTION_SAMPLE_FRQ))
                    _sampling_frq = Int32.Parse(line.Substring(OPTION_SAMPLE_FRQ.Length + 1).TrimStart());
                else if (line.StartsWith(OPTION_FRAME_PRD))
                    _frame_prd = Int32.Parse(line.Substring(OPTION_FRAME_PRD.Length + 1).TrimStart());
                else if (line.StartsWith(OPTION_NUM_STATES))
                    _num_states = Int32.Parse(line.Substring(OPTION_NUM_STATES.Length + 1).TrimStart());
                else if (line.StartsWith(OPTION_NUM_STREAMS))
                    i = Int32.Parse(line.Substring(OPTION_NUM_STREAMS.Length + 1).TrimStart());
                else if (line.StartsWith(OPTION_STREAM_TYPE))
                {
                    string[] streams = line.Split(':');
                    if (streams.Length == 2)
                    {
                        _stream_type = streams[1].TrimStart().Split(',');
                        if (i <= 0)
                        {
                            //num_streams must come before stream_type

                            throw new InvalidDataException();
                        }
                        else if (i != _stream_type.Length)
                        {
                            //num_streams must show the length of stream_type

                            throw new InvalidDataException();
                        }
                        else
                        {
                            if (_stream_type[0] != "MCP")
                            {
                                //MCP must be the first stream
                                throw new InvalidDataException();
                            }
                            if (_stream_type[1] != "LF0")
                            {
                                //LF0 must be the second stream
                                throw new InvalidDataException();
                            }
                            if (i >= 3 && _stream_type[2] != "LPF")
                            {
                                //LPF must be the third stream, if it is provided
                                throw new InvalidDataException();
                            }
                        }
                    }
                    else
                    {
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(OPTION_LABEL_FMT))
                    _label_fmt = line.Substring(OPTION_LABEL_FMT.Length + 1).TrimStart();
                else if (line.StartsWith(OPTION_LABEL_VER))
                    _label_ver = line.Substring(OPTION_LABEL_VER.Length + 1).TrimStart();
                else if (line.StartsWith(OPTION_GV_OFF))
                {
                    string[] gv_off = line.Split(':');
                    if (gv_off.Length == 2)
                        _gv_off = gv_off[1].TrimStart().Split(',');
                    else
                        throw new InvalidDataException();
                }
                else if (line.StartsWith(OPTION_COMMENT))
                    _comment = line.Substring(OPTION_COMMENT.Length + 1).TrimStart();
                else
                {
                    //unknown option
                    throw new InvalidDataException();
                }
            }

            position += line.Length + 1;
            #endregion

            #region Reading STREAM options
            while ((line = reader.ReadLine()) != CHUNK_POSITION)
            {
                position += line.Length + 1;

                string[] tokens = line.Split('[', ']', ':', ',');
                if (line.StartsWith(OPTION_VECTOR_LEN))
                {
                    if (tokens.Length == 4)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                _mcp_vector_len = Int32.Parse(tokens[3]);
                                break;
                            case STREAM_LF0:
                                _lf0_vector_len = Int32.Parse(tokens[3]);
                                break;
                            case STREAM_LPF:
                                _lpf_vector_len = Int32.Parse(tokens[3]);
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(OPTION_IS_MSD))
                {
                    if (tokens.Length == 4)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                _mcp_is_msd = tokens[3] == "1";
                                break;
                            case STREAM_LF0:
                                _lf0_is_msd = tokens[3] == "1";
                                break;
                            case STREAM_LPF:
                                _lpf_is_msd = tokens[3] == "1";
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(OPTION_NUM_WINDOWS))
                {
                    if (tokens.Length == 4)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                _mcp_num_windows = Int32.Parse(tokens[3]);
                                break;
                            case STREAM_LF0:
                                _lf0_num_windows = Int32.Parse(tokens[3]);
                                break;
                            case STREAM_LPF:
                                _lpf_num_windows = Int32.Parse(tokens[3]);
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(OPTION_USE_GV))
                {
                    if (tokens.Length == 4)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                _mcp_use_gv = tokens[3] == "1";
                                break;
                            case STREAM_LF0:
                                _lf0_use_gv = tokens[3] == "1";
                                break;
                            case STREAM_LPF:
                                _lpf_use_gv = tokens[3] == "1";
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(OPTION_STREAM_OPT))
                {
                    if (tokens.Length >= 4)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                if (tokens[3].Length > 0)
                                {
                                    _mcp_options = new string[tokens.Length - 3];
                                    Array.Copy(tokens, 3, _mcp_options, 0, _mcp_options.Length);
                                }
                                else
                                {
                                    _lf0_options = null;
                                }
                                break;
                            case STREAM_LF0:
                                if (tokens[3].Length > 0)
                                {
                                    _lf0_options = new string[tokens.Length - 3];
                                    Array.Copy(tokens, 3, _lf0_options, 0, _lf0_options.Length);
                                }
                                else
                                {
                                    _lf0_options = null;
                                }
                                break;
                            case STREAM_LPF:
                                if (tokens[3].Length > 0)
                                {
                                    _lpf_options = new string[tokens.Length - 3];
                                    Array.Copy(tokens, 3, _lpf_options, 0, _lpf_options.Length);
                                }
                                else
                                {
                                    _lf0_options = null;
                                }
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
            }

            for (i = 0; i < _mcp_options.Length; i++)
            {
                string[] components = _mcp_options[i].Split('=');
                if (components.Length == 2)
                {
                    switch (components[0])
                    {
                        case OPTION_MCP_ALPHA:
                            _mcp_alpha = components[1].Trim();
                            break;
                        case OPTION_MCP_LN_GAIN:
                            _mcp_ln_gain = components[1].Trim();
                            break;
                        case OPTION_MCP_GAMMA:
                            _mcp_gamma = components[1].Trim();
                            break;
                    }
                }
                else
                {
                    //inappropriate number of =
                    throw new InvalidDataException();
                }
            }

            position += line.Length + 1;
            #endregion

            #region Reading subchunk positions
            while ((line = reader.ReadLine()) != CHUNK_DATA)
            {
                position += line.Length + 1;

                string[] tokens = line.Split('[', ']', ':', ',', '-');
                if (line.StartsWith(SUBCHUNK_DUR_PDF))
                {
                    if (tokens.Length == 3)
                        dur_pdf_pos = new Tuple<long, long>(Int64.Parse(tokens[1]), Int64.Parse(tokens[2]));
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_DUR_TREE))
                {
                    if (tokens.Length == 3)
                        dur_tree_pos = new Tuple<long, long>(Int64.Parse(tokens[1]), Int64.Parse(tokens[2]));
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_STM_WIN))
                {
                    if (tokens.Length >= 5)
                    {
                        Tuple<long, long>[] temp = new Tuple<long, long>[(tokens.Length - 3) / 2];
                        for (i = 0; i < temp.Length; i++)
                        {
                            temp[i] = new Tuple<long, long>(
                                Int64.Parse(tokens[i * 2 + 3]),
                                Int64.Parse(tokens[i * 2 + 4]));
                        }
                        switch (tokens[1])
                        {
                            case STREAM_MCP: mcp_win_pos = temp; break;
                            case STREAM_LF0: lf0_win_pos = temp; break;
                            case STREAM_LPF: lpf_win_pos = temp; break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_STM_PDF))
                {
                    if (tokens.Length == 5)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                mcp_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LF0:
                                lf0_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LPF:
                                lpf_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_STM_TREE))
                {
                    if (tokens.Length == 5)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                mcp_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LF0:
                                lf0_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LPF:
                                lpf_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_GV_PDF))
                {
                    if (tokens.Length == 5)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                gv_mcp_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LF0:
                                gv_lf0_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LPF:
                                gv_lpf_pdf_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
                else if (line.StartsWith(SUBCHUNK_GV_TREE))
                {
                    if (tokens.Length == 5)
                    {
                        switch (tokens[1])
                        {
                            case STREAM_MCP:
                                gv_mcp_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LF0:
                                gv_lf0_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                            case STREAM_LPF:
                                gv_lpf_tree_pos =
                                    new Tuple<long, long>(Int64.Parse(tokens[3]), Int64.Parse(tokens[4]));
                                break;
                        }
                    }
                    else
                    {
                        //invalid data
                        throw new InvalidDataException();
                    }
                }
            }

            position += line.Length + 1;
            #endregion
            
            //TextReader works by first reading 1024 bytes from the file and saving it in a RAM
            //After using TextReader to read, the header of the file will be advanced by 1024,
            //not the number of text read.
            htsvoice.Position = position;

            #region Reading subchunk
            //Duration
            dur_pdf = new HTSPDFFile(htsvoice, dur_pdf_pos.Item1, dur_pdf_pos.Item2,
                1, false, 1, _num_states);
            dur_tree = new HTSTreeFile(htsvoice, dur_tree_pos.Item1, dur_tree_pos.Item2);

            //Streams and GVs
            if (_stream_type[0] == STREAM_MCP)
            {
                mcp_windows = new HTSWindowFiles(htsvoice, mcp_win_pos);
                mcp_pdf = new HTSPDFFile(htsvoice, mcp_pdf_pos.Item1, mcp_pdf_pos.Item2,
                    _num_states, _mcp_is_msd, _mcp_num_windows, _mcp_vector_len);
                mcp_tree = new HTSTreeFile(htsvoice, mcp_tree_pos.Item1, mcp_tree_pos.Item2);

                if (_mcp_use_gv)
                {
                    gv_mcp_pdf = new HTSPDFFile(htsvoice, gv_mcp_pdf_pos.Item1, gv_mcp_pdf_pos.Item2,
                        1, false, 1, _mcp_vector_len);
                    gv_mcp_tree = new HTSTreeFile(htsvoice, gv_mcp_tree_pos.Item1, gv_mcp_tree_pos.Item2);
                }
            }
            if (_stream_type[1] == STREAM_LF0)
            {
                lf0_windows = new HTSWindowFiles(htsvoice, lf0_win_pos);
                lf0_pdf = new HTSPDFFile(htsvoice, lf0_pdf_pos.Item1, lf0_pdf_pos.Item2,
                    _num_states, _lf0_is_msd, _lf0_num_windows, _lf0_vector_len);
                lf0_tree = new HTSTreeFile(htsvoice, lf0_tree_pos.Item1, lf0_tree_pos.Item2);

                if (_lf0_use_gv)
                {
                    gv_lf0_pdf = new HTSPDFFile(htsvoice, gv_lf0_pdf_pos.Item1, gv_lf0_pdf_pos.Item2,
                        1, false, 1, _lf0_vector_len);
                    gv_lf0_tree = new HTSTreeFile(htsvoice, gv_lf0_tree_pos.Item1, gv_lf0_tree_pos.Item2);
                }
            }
            if (_stream_type.Length >= 2 && _stream_type[2] == STREAM_LPF)
            {
                lpf_windows = new HTSWindowFiles(htsvoice, lpf_win_pos);
                lpf_pdf = new HTSPDFFile(htsvoice, lpf_pdf_pos.Item1, lpf_pdf_pos.Item2,
                    _num_states, _lpf_is_msd, _lpf_num_windows, _lpf_vector_len);
                lpf_tree = new HTSTreeFile(htsvoice, lpf_tree_pos.Item1, lpf_tree_pos.Item2);

                if (_lpf_use_gv)
                {
                    gv_lpf_pdf = new HTSPDFFile(htsvoice, gv_lpf_pdf_pos.Item1, gv_lpf_pdf_pos.Item2,
                        1, false, 1, _lpf_vector_len);
                    gv_lpf_tree = new HTSTreeFile(htsvoice, gv_lpf_tree_pos.Item1, gv_lpf_tree_pos.Item2);
                }
            }
            
            #endregion

            htsvoice.Close();
        }

        public string HTSVoiceVer { get { return _htsvoice_ver; } }
        public int SamplingFrequency { get { return _sampling_frq; } set { _sampling_frq = value; } }
        public int FramePeriod { get { return _frame_prd; } set { _frame_prd = value; } }
        public int NumStates { get { return _num_states; } set { SetNumStates(value); } }
        public int NumStreams { get { return _stream_type.Length; } }
        public string[] StreamType { get { return _stream_type; } }
        public string StreamTypeString { get { return String.Join(",", _stream_type); } }
        public bool UseLPFStream { get { return StreamType.Length == 3; }
            set
            {
                if (value && StreamType.Length == 2)
                    _stream_type = new string[3] { STREAM_MCP, STREAM_LF0, STREAM_LPF };
                else if (!value && StreamType.Length == 3)
                    _stream_type = new string[2] { STREAM_MCP, STREAM_LF0};
            }
        }
        public string LabelFormat { get { return _label_fmt; } set { _label_fmt = value; } }
        public string LabelVersion { get { return _label_ver; } set { _label_ver = value; } }
        public string[] GVOff { get { return _gv_off; } set { _gv_off = value; } }
        public string GVOffForFile { get { return String.Join(",", _gv_off); } }
        public string GVOffForEditing { get { return String.Join(Environment.NewLine, _gv_off); } }
        public string Comment { get { return _comment; } set { _comment = value; } }

        //this function is far reaching
        public void SetNumStates(int new_num_states)
        {
        }

        public void SetGVOffByEditing(string text)
        {
            GVOff = text.Split(new char[]{'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
