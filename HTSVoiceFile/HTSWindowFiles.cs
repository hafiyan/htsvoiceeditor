﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace FileBasicAnalysis
{
    public class HTSWindowFiles
    {
        List<HTSWindowFile> _windows;

        public HTSWindowFiles(FileStream htsvoice, Tuple<long, long>[] positions)
        {
            _windows = new List<HTSWindowFile>();

            for (int i = 0; i < positions.Length; i++)
            {
                _windows.Add(new HTSWindowFile(htsvoice, positions[i].Item1, positions[i].Item2));
            }
        }
    }
}
