﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Threading;
using System.Globalization;
using HTSVoiceEditor.Properties;
using FileBasicAnalysis;

namespace HTSVoiceEditor
{
    public partial class MainForm : Form
    {
        private class FileCondition
        {
            public static string FileName = null;
            public static string TempFileName = null;
            public static bool Changed = false;
        }

        HTSVoiceFile htsvoice = null;
        TabPage tab_PDF_base = null;

        public MainForm()
        {
            InitializeComponent();

            globalOptionsViewer.HTSVoiceFile = htsvoice;
        }

        public MainForm(string path)
            : this()
        {
            //MessageBox.Show(path);
            FileCondition.FileName = path;
            FileCondition.TempFileName = Path.GetTempFileName();

            File.Copy(FileCondition.FileName, FileCondition.TempFileName, true);
            htsvoice = new HTSVoiceFile(FileCondition.TempFileName);

            //HTSVoiceFile file = new HTSVoiceFile(@"C:\cygwin\home\Hafiyan\htsvoices\mei_normal.htsvoice");

            globalOptionsViewer.HTSVoiceFile = htsvoice;

            //Capturing tabs
            tab_PDF_base = tabPDFBase;
            tabEditing.TabPages.RemoveAt(1);
        }

        private void btnNewFile_Click(object sender, EventArgs e)
        {
            //http://stackoverflow.com/questions/1142802/how-to-use-localization-in-c-sharp
            //Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");

            System.Diagnostics.Process.Start(Application.ExecutablePath);
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            DialogResult answer = DialogResult.None;

            if (FileCondition.FileName != null && FileCondition.Changed)
            {
                answer = MessageBox.Show(Resources.msgSaveFile, Resources.strHTSVoiceEditor,
                    MessageBoxButtons.YesNoCancel);
            }

            switch (answer)
            {
                case DialogResult.Yes:
                    //save the file first

                    goto case DialogResult.No;
                case DialogResult.None:
                case DialogResult.No:
                    //open the file
                    break;
                default:
                    break;
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (FileCondition.FileName == null)
            {
            }
            else
            {
                this.Text = Resources.strHTSVoiceEditor + " - " + FileCondition.FileName;
            }
        }

        private void mnuComponents_Opening(object sender, CancelEventArgs e)
        {
            TreeNode node = treComponents.SelectedNode;

            if (GetComponentName(node.FullPath) != "")
            {
                openToolStripMenuItem.Visible = true;
                openToolStripMenuItem.Text = String.Format(Resources.menuitemOpen,
                    GetComponentName(node.FullPath));
            }
            else
            {
                openToolStripMenuItem.Visible = false;
            }
            //MessageBox.Show(node.FullPath);
        }

        private void treComponents_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                treComponents.SelectedNode = e.Node;
            }
        }
        
        private string GetComponentName(string node_path)
        {
            switch (node_path)
            {
                case "DURATION":
                case "STREAM":
                case "GV":
                case @"GV\MCP":
                case @"GV\LF0":
                case @"GV\LPF":
                    return "";
                case @"DURATION\PDF": return "DURATION_PDF";
                case @"DURATION\TREE": return "DURATION_TREE";
                case @"STREAM\MCP": return "MCP";
                case @"STREAM\MCP\WIN": return "STREAM_WIN[MCP]";
                case @"STREAM\MCP\PDF": return "STREAM_PDF[MCP]";
                case @"STREAM\MCP\TREE": return "STREAM_TREE[MCP]";
                case @"STREAM\LF0": return "LF0";
                case @"STREAM\LF0\WIN": return "STREAM_WIN[LF0]";
                case @"STREAM\LF0\PDF": return "STREAM_PDF[LF0]";
                case @"STREAM\LF0\TREE": return "STREAM_TREE[LF0]";
                case @"STREAM\LPF": return "LPF";
                case @"STREAM\LPF\WIN": return "STREAM_WIN[LPF]";
                case @"STREAM\LPF\PDF": return "STREAM_PDF[LPF]";
                case @"STREAM\LPF\TREE": return "STREAM_TREE[LPF]";
                case @"GV\MCP\PDF": return "GV_PDF[MCP]";
                case @"GV\MCP\TREE": return "GV_TREE[MCP]";
                case @"GV\LF0\PDF": return "GV_PDF[LF0]";
                case @"GV\LF0\TREE": return "GV_TREE[LF0]";
                case @"GV\LPF\PDF": return "GV_PDF[LPF]";
                case @"GV\LPF\TREE": return "GV_TREE[LPF]";
            }

            return node_path;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TabPage new_tab = null;

            //tabEditing.TabPages.Add("");
            if (openToolStripMenuItem.Text == String.Format(Resources.menuitemOpen, "DURATION_PDF"))
            {
                //cloning must be done here
                new_tab = tab_PDF_base;

                tabEditing.TabPages.Add(tab_PDF_base);
            }
        }

        private void globalOptionsViewer_Changed(object sender, EventArgs e)
        {
            FileCondition.Changed = true;
        }
    }
}
