﻿namespace HTSVoiceEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("DURATION", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("WIN");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("MCP", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5,
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("WIN");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("LF0", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("WIN");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("LPF", new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13,
            treeNode14});
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("STREAM", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode11,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("MCP", new System.Windows.Forms.TreeNode[] {
            treeNode17,
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("LF0", new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21});
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("PDF");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("TREE");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("LPF", new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("GV", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode22,
            treeNode25});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNewFile = new System.Windows.Forms.ToolStripButton();
            this.btnOpenFile = new System.Windows.Forms.ToolStripButton();
            this.btnSaveFile = new System.Windows.Forms.ToolStripButton();
            this.btnSaveFileAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnTest = new System.Windows.Forms.ToolStripButton();
            this.btnTestOptions = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnAbout = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treComponents = new System.Windows.Forms.TreeView();
            this.mnuComponents = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imgTree = new System.Windows.Forms.ImageList(this.components);
            this.tabEditing = new System.Windows.Forms.TabControl();
            this.tabGlobalPage = new System.Windows.Forms.TabPage();
            this.tabPDFBase = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.globalOptionsViewer = new HTSFilesViewer.GlobalOptionsViewer();
            this.probDensFuncViewer1 = new HTSFilesViewer.ProbDensFuncViewer();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.mnuComponents.SuspendLayout();
            this.tabEditing.SuspendLayout();
            this.tabGlobalPage.SuspendLayout();
            this.tabPDFBase.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNewFile,
            this.btnOpenFile,
            this.btnSaveFile,
            this.btnSaveFileAs,
            this.toolStripSeparator1,
            this.btnTest,
            this.btnTestOptions,
            this.toolStripSeparator2,
            this.btnHelp,
            this.btnAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(872, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNewFile
            // 
            this.btnNewFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNewFile.Image = global::HTSVoiceEditor.Properties.Resources.new_icon;
            this.btnNewFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewFile.Name = "btnNewFile";
            this.btnNewFile.Size = new System.Drawing.Size(36, 36);
            this.btnNewFile.Text = global::HTSVoiceEditor.Properties.Resources.btnNewFile;
            this.btnNewFile.Click += new System.EventHandler(this.btnNewFile_Click);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOpenFile.Image = global::HTSVoiceEditor.Properties.Resources.open_icon;
            this.btnOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(36, 36);
            this.btnOpenFile.Text = global::HTSVoiceEditor.Properties.Resources.btnOpenFile;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveFile.Image = global::HTSVoiceEditor.Properties.Resources.save_icon;
            this.btnSaveFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(36, 36);
            this.btnSaveFile.Text = global::HTSVoiceEditor.Properties.Resources.btnSaveFile;
            // 
            // btnSaveFileAs
            // 
            this.btnSaveFileAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveFileAs.Image = global::HTSVoiceEditor.Properties.Resources.save_as_icon;
            this.btnSaveFileAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveFileAs.Name = "btnSaveFileAs";
            this.btnSaveFileAs.Size = new System.Drawing.Size(36, 36);
            this.btnSaveFileAs.Text = global::HTSVoiceEditor.Properties.Resources.btnSaveFileAs;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // btnTest
            // 
            this.btnTest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTest.Image = global::HTSVoiceEditor.Properties.Resources.test_play_icon;
            this.btnTest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(36, 36);
            this.btnTest.Text = global::HTSVoiceEditor.Properties.Resources.btnTest;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnTestOptions
            // 
            this.btnTestOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTestOptions.Image = global::HTSVoiceEditor.Properties.Resources.test_options_icon;
            this.btnTestOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTestOptions.Name = "btnTestOptions";
            this.btnTestOptions.Size = new System.Drawing.Size(36, 36);
            this.btnTestOptions.Text = global::HTSVoiceEditor.Properties.Resources.btnTestOptions;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = global::HTSVoiceEditor.Properties.Resources.help_icon;
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(36, 36);
            this.btnHelp.Text = global::HTSVoiceEditor.Properties.Resources.btnHelp;
            // 
            // btnAbout
            // 
            this.btnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAbout.Image = global::HTSVoiceEditor.Properties.Resources.app_icon_png;
            this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(36, 36);
            this.btnAbout.Text = global::HTSVoiceEditor.Properties.Resources.btnAbout;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 39);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treComponents);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabEditing);
            this.splitContainer1.Size = new System.Drawing.Size(872, 525);
            this.splitContainer1.SplitterDistance = 289;
            this.splitContainer1.TabIndex = 1;
            // 
            // treComponents
            // 
            this.treComponents.ContextMenuStrip = this.mnuComponents;
            this.treComponents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treComponents.ImageIndex = 0;
            this.treComponents.ImageList = this.imgTree;
            this.treComponents.Location = new System.Drawing.Point(0, 0);
            this.treComponents.Name = "treComponents";
            treeNode1.ImageKey = "pdf_icon.png";
            treeNode1.Name = "nodPDF";
            treeNode1.SelectedImageKey = "pdf_icon.png";
            treeNode1.Text = "PDF";
            treeNode2.ImageKey = "tree_icon.png";
            treeNode2.Name = "nodTree";
            treeNode2.SelectedImageKey = "tree_icon.png";
            treeNode2.Text = "TREE";
            treeNode3.ImageIndex = 0;
            treeNode3.Name = "nodDuration";
            treeNode3.Text = "DURATION";
            treeNode4.ImageKey = "win_icon.png";
            treeNode4.Name = "nodWindow";
            treeNode4.SelectedImageKey = "win_icon.png";
            treeNode4.Text = "WIN";
            treeNode5.ImageKey = "pdf_icon.png";
            treeNode5.Name = "nodPDF";
            treeNode5.SelectedImageKey = "pdf_icon.png";
            treeNode5.Text = "PDF";
            treeNode6.ImageKey = "tree_icon.png";
            treeNode6.Name = "nodTree";
            treeNode6.SelectedImageKey = "tree_icon.png";
            treeNode6.Text = "TREE";
            treeNode7.ImageKey = "mcp_icon.png";
            treeNode7.Name = "nodMCP";
            treeNode7.SelectedImageKey = "mcp_icon.png";
            treeNode7.Text = "MCP";
            treeNode8.ImageKey = "win_icon.png";
            treeNode8.Name = "nodWindow";
            treeNode8.SelectedImageKey = "win_icon.png";
            treeNode8.Text = "WIN";
            treeNode9.ImageKey = "pdf_icon.png";
            treeNode9.Name = "nodPDF";
            treeNode9.SelectedImageKey = "pdf_icon.png";
            treeNode9.Text = "PDF";
            treeNode10.ImageKey = "tree_icon.png";
            treeNode10.Name = "nodTree";
            treeNode10.SelectedImageKey = "tree_icon.png";
            treeNode10.Text = "TREE";
            treeNode11.ImageKey = "lf0_icon.png";
            treeNode11.Name = "nodLF0";
            treeNode11.SelectedImageKey = "lf0_icon.png";
            treeNode11.Text = "LF0";
            treeNode12.ImageKey = "win_icon.png";
            treeNode12.Name = "nodWindow";
            treeNode12.SelectedImageKey = "win_icon.png";
            treeNode12.Text = "WIN";
            treeNode13.ImageKey = "pdf_icon.png";
            treeNode13.Name = "nodPDF";
            treeNode13.SelectedImageKey = "pdf_icon.png";
            treeNode13.Text = "PDF";
            treeNode14.ImageKey = "tree_icon.png";
            treeNode14.Name = "nodTree";
            treeNode14.SelectedImageKey = "tree_icon.png";
            treeNode14.Text = "TREE";
            treeNode15.ImageKey = "opt_lpf_icon.png";
            treeNode15.Name = "nodLPF";
            treeNode15.SelectedImageKey = "opt_lpf_icon.png";
            treeNode15.Text = "LPF";
            treeNode16.ImageKey = "stream_icon.png";
            treeNode16.Name = "nodStream";
            treeNode16.SelectedImageKey = "stream_icon.png";
            treeNode16.Text = "STREAM";
            treeNode17.ImageKey = "pdf_icon.png";
            treeNode17.Name = "nodPDF";
            treeNode17.SelectedImageKey = "pdf_icon.png";
            treeNode17.Text = "PDF";
            treeNode18.ImageKey = "tree_icon.png";
            treeNode18.Name = "nodTree";
            treeNode18.SelectedImageKey = "tree_icon.png";
            treeNode18.Text = "TREE";
            treeNode19.ImageKey = "opt_mcp_icon.png";
            treeNode19.Name = "nodMCP";
            treeNode19.SelectedImageKey = "opt_mcp_icon.png";
            treeNode19.Text = "MCP";
            treeNode20.ImageKey = "pdf_icon.png";
            treeNode20.Name = "nodPDF";
            treeNode20.SelectedImageKey = "pdf_icon.png";
            treeNode20.Text = "PDF";
            treeNode21.ImageKey = "tree_icon.png";
            treeNode21.Name = "nodTree";
            treeNode21.SelectedImageKey = "tree_icon.png";
            treeNode21.Text = "TREE";
            treeNode22.ImageKey = "opt_lf0_icon.png";
            treeNode22.Name = "nodLF0";
            treeNode22.SelectedImageKey = "opt_lf0_icon.png";
            treeNode22.Text = "LF0";
            treeNode23.ImageKey = "pdf_icon.png";
            treeNode23.Name = "nodPDF";
            treeNode23.SelectedImageKey = "pdf_icon.png";
            treeNode23.Text = "PDF";
            treeNode24.ImageKey = "tree_icon.png";
            treeNode24.Name = "nodTree";
            treeNode24.SelectedImageKey = "tree_icon.png";
            treeNode24.Text = "TREE";
            treeNode25.ImageKey = "empty.png";
            treeNode25.Name = "nodLPF";
            treeNode25.SelectedImageKey = "empty.png";
            treeNode25.Text = "LPF";
            treeNode26.ImageKey = "gv_icon.png";
            treeNode26.Name = "nodGV";
            treeNode26.SelectedImageKey = "gv_icon.png";
            treeNode26.Text = "GV";
            this.treComponents.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode16,
            treeNode26});
            this.treComponents.SelectedImageIndex = 0;
            this.treComponents.Size = new System.Drawing.Size(289, 525);
            this.treComponents.TabIndex = 0;
            this.treComponents.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treComponents_NodeMouseClick);
            // 
            // mnuComponents
            // 
            this.mnuComponents.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.importToolStripMenuItem,
            this.createNewToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.mnuComponents.Name = "mnuComponents";
            this.mnuComponents.Size = new System.Drawing.Size(138, 114);
            this.mnuComponents.Opening += new System.ComponentModel.CancelEventHandler(this.mnuComponents_Opening);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Meiryo", 9F, System.Drawing.FontStyle.Bold);
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.openToolStripMenuItem.Text = global::HTSVoiceEditor.Properties.Resources.menuitemOpen;
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.exportToolStripMenuItem.Text = global::HTSVoiceEditor.Properties.Resources.menuitemExport;
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Enabled = false;
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.importToolStripMenuItem.Text = global::HTSVoiceEditor.Properties.Resources.menuitemImport;
            // 
            // createNewToolStripMenuItem
            // 
            this.createNewToolStripMenuItem.Enabled = false;
            this.createNewToolStripMenuItem.Name = "createNewToolStripMenuItem";
            this.createNewToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.createNewToolStripMenuItem.Text = global::HTSVoiceEditor.Properties.Resources.menuitemNew;
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.deleteToolStripMenuItem.Text = global::HTSVoiceEditor.Properties.Resources.menuitemDelete;
            // 
            // imgTree
            // 
            this.imgTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgTree.ImageStream")));
            this.imgTree.TransparentColor = System.Drawing.Color.Magenta;
            this.imgTree.Images.SetKeyName(0, "duration_icon.png");
            this.imgTree.Images.SetKeyName(1, "stream_icon.png");
            this.imgTree.Images.SetKeyName(2, "gv_icon.png");
            this.imgTree.Images.SetKeyName(3, "mcp_icon.png");
            this.imgTree.Images.SetKeyName(4, "lf0_icon.png");
            this.imgTree.Images.SetKeyName(5, "lpf_icon.png");
            this.imgTree.Images.SetKeyName(6, "win_icon.png");
            this.imgTree.Images.SetKeyName(7, "pdf_icon.png");
            this.imgTree.Images.SetKeyName(8, "tree_icon.png");
            this.imgTree.Images.SetKeyName(9, "opt_mcp_icon.png");
            this.imgTree.Images.SetKeyName(10, "opt_lf0_icon.png");
            this.imgTree.Images.SetKeyName(11, "opt_lpf_icon.png");
            this.imgTree.Images.SetKeyName(12, "empty.png");
            // 
            // tabEditing
            // 
            this.tabEditing.Controls.Add(this.tabGlobalPage);
            this.tabEditing.Controls.Add(this.tabPDFBase);
            this.tabEditing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabEditing.Location = new System.Drawing.Point(0, 0);
            this.tabEditing.Name = "tabEditing";
            this.tabEditing.SelectedIndex = 0;
            this.tabEditing.Size = new System.Drawing.Size(579, 525);
            this.tabEditing.TabIndex = 0;
            // 
            // tabGlobalPage
            // 
            this.tabGlobalPage.Controls.Add(this.globalOptionsViewer);
            this.tabGlobalPage.Location = new System.Drawing.Point(4, 22);
            this.tabGlobalPage.Name = "tabGlobalPage";
            this.tabGlobalPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabGlobalPage.Size = new System.Drawing.Size(571, 499);
            this.tabGlobalPage.TabIndex = 0;
            this.tabGlobalPage.Text = "[GLOBAL]";
            this.tabGlobalPage.UseVisualStyleBackColor = true;
            // 
            // tabPDFBase
            // 
            this.tabPDFBase.Controls.Add(this.tableLayoutPanel1);
            this.tabPDFBase.Location = new System.Drawing.Point(4, 22);
            this.tabPDFBase.Name = "tabPDFBase";
            this.tabPDFBase.Padding = new System.Windows.Forms.Padding(3);
            this.tabPDFBase.Size = new System.Drawing.Size(571, 499);
            this.tabPDFBase.TabIndex = 1;
            this.tabPDFBase.Text = "tabPDFBase";
            this.tabPDFBase.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.probDensFuncViewer1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(565, 493);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(81, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // globalOptionsViewer
            // 
            this.globalOptionsViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.globalOptionsViewer.HTSVoiceFile = null;
            this.globalOptionsViewer.Location = new System.Drawing.Point(3, 3);
            this.globalOptionsViewer.Name = "globalOptionsViewer";
            this.globalOptionsViewer.Size = new System.Drawing.Size(565, 493);
            this.globalOptionsViewer.TabIndex = 0;
            this.globalOptionsViewer.Changed += new System.EventHandler(this.globalOptionsViewer_Changed);
            // 
            // probDensFuncViewer1
            // 
            this.probDensFuncViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.probDensFuncViewer1.Location = new System.Drawing.Point(3, 38);
            this.probDensFuncViewer1.Name = "probDensFuncViewer1";
            this.probDensFuncViewer1.Size = new System.Drawing.Size(559, 412);
            this.probDensFuncViewer1.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 564);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "HTS Voice Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.mnuComponents.ResumeLayout(false);
            this.tabEditing.ResumeLayout(false);
            this.tabGlobalPage.ResumeLayout(false);
            this.tabPDFBase.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNewFile;
        private System.Windows.Forms.ToolStripButton btnOpenFile;
        private System.Windows.Forms.ToolStripButton btnSaveFile;
        private System.Windows.Forms.ToolStripButton btnSaveFileAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnTest;
        private System.Windows.Forms.ToolStripButton btnTestOptions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnHelp;
        private System.Windows.Forms.ToolStripButton btnAbout;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabEditing;
        private System.Windows.Forms.TabPage tabGlobalPage;
        private System.Windows.Forms.TreeView treComponents;
        private System.Windows.Forms.ImageList imgTree;
        private System.Windows.Forms.ContextMenuStrip mnuComponents;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private HTSFilesViewer.GlobalOptionsViewer globalOptionsViewer;
        private System.Windows.Forms.TabPage tabPDFBase;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private HTSFilesViewer.ProbDensFuncViewer probDensFuncViewer1;
    }
}

