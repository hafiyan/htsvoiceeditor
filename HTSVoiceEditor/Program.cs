﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Diagnostics;

namespace HTSVoiceEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string[] x = Environment.GetCommandLineArgs();
            if (x.Length <= 1)
            {
                Application.Run(new MainForm());
            }
            else if (x.Length == 2)
            {
                Application.Run(new MainForm(x[1]));
            }
            else
            {
                ProcessStartInfo psi = new ProcessStartInfo(x[0]);
                string option = "";
                for (int i = 2; i < x.Length; i++)
                {
                    option += '\"';
                    option += x[i];
                    option += '\"';

                    if (i < x.Length - 1) option += ' ';
                }
                psi.Arguments = option;
                Process.Start(psi);

                Application.Run(new MainForm(x[1]));
            }
        }
    }
}
